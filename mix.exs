defmodule PgpWordlist.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/bfcarpio/elixir-pgp-wordlist"

  def project do
    [
      app: :pgp_wordlist,
      version: "0.3.0",
      elixir: "~> 1.9",
      description: description(),
      package: package(),
      source_url: @source_url,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp description do
    """
    Library for converting hex to / from the PGP word list: https://en.wikipedia.org/wiki/PGP_word_list
    """
  end

  defp package do
    [
      name: :pgp_wordlist,
      files: ["lib", "mix.exs", "README*", "LICENSE"],
      maintainers: ["Brendan Carpio"],
      licenses: ["MIT"],
      links: %{GitLab: @source_url}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.3", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21.3", only: [:dev]},
      {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false}
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: ["README.md"]
    ]
  end
end

# PgpWordlist
[![pipeline status](https://gitlab.com/bfcarpio/elixir-pgp-wordlist/badges/master/pipeline.svg)](https://gitlab.com/bfcarpio/elixir-pgp-wordlist/-/commits/master)
[![coverage report](https://gitlab.com/bfcarpio/elixir-pgp-wordlist/badges/master/coverage.svg)](https://gitlab.com/bfcarpio/elixir-pgp-wordlist/-/commits/master)
[![Open Source Love svg3](https://badges.frapsoft.com/os/v3/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![MIT Licence](https://img.shields.io/hexpm/l/exnoops.svg)](https://opensource.org/licenses/mit-license.php)
[![Hex.pm](https://img.shields.io/hexpm/v/pgp_wordlist.svg)](https://hex.pm/packages/pgp_wordlist)


Library for converting hex to / from the PGP word list.

Word source: https://en.wikipedia.org/wiki/PGP_word_list


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `pgp_wordlist` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:pgp_wordlist, "~> 0.3.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/pgp_wordlist](https://hexdocs.pm/pgp_wordlist).

